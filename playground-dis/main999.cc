// main999.cc is a part of the PYTHIA event generator.
// First developed with Pythia version 8.235
// This is an extremely simple test program to be used with an external input
// file, which can be run like
// ./main999.exe myInputs.cmnd > myLogfile

#include "Pythia8/Pythia.h"

using namespace Pythia8;

//==========================================================================

int main(int argc, char* argv[]) {

  // Confirm that external file will be used for settings..
  cout << " PYTHIA settings will be read from file " << argv[1] << endl;

  // Declare generator. Read in commands from external file.
  Pythia pythia;
  pythia.readFile(argv[1]);

  // Initialization.
  pythia.init();

  Hist mult;
  mult.book( "charged multiplicity", 100, -0.5, 99.5);

  // Read in number of event and maximal number of aborts.
  int nEvent = pythia.mode("Main:numberOfEvents");
  int nAbort = 100;

  // Begin event loop.
  int iAbort = 0;
  for (int iEvent = 0; iEvent < nEvent; ++iEvent) {

    // Generate events. Quit if too many failures.
    if (!pythia.next()) {
      if (++iAbort < nAbort) continue;
      cout << " Event generation aborted prematurely, owing to error!\n";
      break;
    }

    // Plot pseudorapidity distribution. Sum up charged multiplicity.
    int nChg = 0;
    for (int i = 0; i < pythia.event.size(); ++i)
    if (pythia.event[i].isFinal() && pythia.event[i].isCharged()) ++nChg;
    mult.fill( nChg );

  // End of event loop.
  }

  // Final statistics.
  pythia.stat();

  // Normalize histograms.
  double norm = 1.;
  mult *= norm;

  // Print histograms.
  cout << mult;

  // Write histograms to file
  ofstream write;
  write.open("pythia-multi.dat");
  mult.table(write);
  write.close();

  // A simple plotting command.
  //gnuplot -e "plot 'pythia-multi.dat' using 1:2 w boxes title 'Pythia', 'dire-multi.dat' using 1:2 w lines title 'Dire'; pause -1"

  // Done.
  return 0;
}
